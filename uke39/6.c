int main(void)
{
	/* Hvis fork returnerer > 0 så er vi i foreldreprosessen
		Kodesnutten vil ha effekt av å skifte PID på prosessen.
		Barnet blir en eksakt kopi av forelderen, og fortsetter
		der den slapp.
	*/
	if(fork() > 0) exit(0);
}
