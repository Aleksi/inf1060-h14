#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

#include <sched.h>

void random_wait(void)
{
	srand(time(NULL)*getpid());

	int r = rand();
	r = r % 6;

	printf("Random wait %d\n", r);

	sleep(r);
}

void sumfork(int start, int stop, const char* filename)
{
	pid_t p = fork();
	if(p == 0)
	{
		int sum = 0;
		while(start <= stop)
		{
			sum += start;
			start++;
		}
		
		random_wait();

		FILE* f = fopen(filename, "w");
		if(!f)
		{
			perror("fopen");
			exit(0);
		}

		fprintf(f, "%d", sum);
		fclose(f);
		exit(sum);
	}

	printf("Child process pid=%d\n", p);
}

int read_int(const char* filename)
{
	int num;
	FILE* f = fopen(filename, "r");
	if(!f)
	{
		perror("fopen");
		exit(-1);
	}

	fscanf(f, "%d", &num);
	fclose(f);
	return num;
}

int main(void)
{
	int status;
	pid_t p;
	//int *status2 = malloc(sizeof(int));
	
	sumfork(1, 3, "test1.txt");
	sumfork(4, 8, "test2.txt");
	sumfork(9, 10, "test3.txt");

	p = wait(&status);
	printf("Wait done, pid=%d, status=%d\n", p, WEXITSTATUS(status));

	int sum = WEXITSTATUS(status);

	wait(&status);
	sum += WEXITSTATUS(status);
	wait(&status);
	sum += WEXITSTATUS(status);

	//int sum = read_int("test1.txt");
	//sum += read_int("test2.txt");
	//sum += read_int("test3.txt");

	printf("Sum=%d\n", sum);

	/* veldig vanlig å se i kode:
		p = fork();
		if(p == 0)
			exec("gcc");
	*/

	return 0;
}
