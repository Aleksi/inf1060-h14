#include <stdio.h>

void sum(int a, int b, int *result)
{
	// result == 0xfffff010
	// *result == samme som sum2 alltid
	*result = a+b;
}


int main(void)
{
	int a = 5;
	int b = 3;
	int sum2;

	sum(a, b, &sum2);

	printf("%d+%d=%d\n", a, b, sum2);
	return 0;
}
