#include <stdio.h>

struct person
{
	char* name;
	int age;
};

void p_setName(struct person *p, char *name)
{
	if(name == NULL || strlen(name) > 30)
		return 0;

	p->name = name;
	return 1;
}

int p_setAge(struct person *p, int age)
{
	if(age > 200 || age < 1)
		return 0;
	p->age = age;
	return 1; 
}

char* p_getName(struct person *p)
{
	//return (*p).name;
	return p->name;
}

int p_getAge(struct person *p)
{
	return p->age;
}

int p_isSet(struct person *p)
{
	if(p->age <= 0)
		return 0;
	
	if(p->name == NULL)
		return 0;

	return 1;
}

char *p_getInfo(struct person *p)
{
	char *buf = malloc(1024);
	sprintf(buf, "%d:%s", p->age, p->name);
	return buf;
}

int main(void)
{

}

