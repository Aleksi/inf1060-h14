#include <stdio.h>
#include <string.h>

#define STR "hallo"

int main(void)
{
	char buf[1024];
	int antall = 0;

	FILE *f = fopen("tekst.txt", "r");

	if(f == NULL)
	{
		perror("fopen failed");
		return -1;
	}

	while(fgets(buf, 1024, f) != NULL)
	{
		char *tok = strtok(buf, " \n");
		
		do //while(tok)
		{
			if(strcmp(STR, tok) == 0)
			{
				printf("Fant ord!\n");
				antall++;
			}

			tok = strtok(NULL, " \n");
		} while(tok != NULL);
	}

	printf("Antall forekomster: %d\n", antall);

	fclose(f);

	return 0;
}
