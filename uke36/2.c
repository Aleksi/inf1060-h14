#include <stdio.h>
#include <stdlib.h>

char *tulloc(size_t size)
{
	char* buf = malloc(size*4+ 1);

	if(buf == NULL)
	{
		perror("malloc failed");
		exit(-1);
	}

	size_t i;

	for(i = 0; i < size; ++i)
	{
		buf[i*4] = 't';
		buf[(i*4)+1] = 'u';
		buf[(i*4)+2] = buf[(i*4)+3] = 'l';

		//memcpy(buf+(i*4), "tull", 4);
	}

	buf[size*4] = '\0';

	return buf;
}


int main(void)
{
	char *tull = tulloc(10);

	char a[10];

	printf("Pekeren inneholder: %s\n", tull);

	return 0;
}
