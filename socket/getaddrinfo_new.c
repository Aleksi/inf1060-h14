#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>

int main(int argc, char* argv[])
{
	if(argc != 2)
	{
		printf("USAGE: %s [hostname]\n", argv[0]);
		return 0;
	}	

	const char *hostname = argv[1];
	struct addrinfo hints;
	struct addrinfo *res;

	memset(&hints, 0, sizeof(hints));

	hints.ai_family = AF_UNSPEC; // Evnt: AF_INET6 eller AF_UNSPEC
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = 0;

	//IPv4: 192.168.0.1
	//IPv6: fe80::1

	int retv = getaddrinfo(hostname, "80", &hints, &res);

	if(retv != 0)
	{
		printf("getaddrinfo failed: %s\n", gai_strerror(retv));
		return -1;
	}

	struct addrinfo *cur = res;
	while(cur)
	{
		/*if(cur->ai_family != AF_INET)
		{
			cur = cur->ai_next;
			continue;
		}*/

		/*
		For å koble til en remote-maskin:
		connect(fd, cur->ai_addr, cur->ai_addrlen);
		*/


		//IPv6: struct sockaddr_in6
		if(cur->ai_family == AF_INET)
		{
			struct sockaddr_in *cur_addr = (struct sockaddr_in*)cur->ai_addr;
			char buf[100];
			buf[0] = 0;

			inet_ntop(AF_INET, &(cur_addr->sin_addr), buf, sizeof(buf));
			printf("IP address: %s\n", buf);
		} 
		else if(cur->ai_family == AF_INET6)
		{
			struct sockaddr_in6* cur_addr = (struct sockaddr_in6*)cur->ai_addr;
			char buf[100];
			buf[0] = 0;
		
			inet_ntop(AF_INET6, &(cur_addr->sin6_addr), buf, sizeof(buf));
			printf("IPv6 address: %s\n", buf);
		}
		
		cur = cur->ai_next;
	}
	

	freeaddrinfo(res);
	return 0;
}
