#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[], char* envp[])
{
	int i = 0;
	while(envp[i] != NULL)
	{
		printf("%s\n", envp[i]);
		i++;
	}

	printf("Username is %s\n", getenv("USERNAME"));

	return 0;
}
