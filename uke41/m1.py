#HEI
# Oppgave 1.B
# Run it with:
# python measure.py

def fcfs(prev, tracks):
	# Return tracks in the same order they came in:
    for x in tracks:
        yield x

def scan(prev, tracks):
	# Make two lists: one with all tracks bigger than 50 in ascending order,
	# and one with all tracks smaller than 50, in descending order:
	pieces = [[x for x in tracks if x>=prev], [x for x in tracks if x<prev]]

	pieces[0].sort()
	pieces[1].sort()
	pieces[1].reverse()

	# Join these two arrays to make a full array with sorted tracks:
	full = pieces[0]
	full.extend(pieces[1])

	# Return tracks:
	for x in full:
		yield x

import math

def measure(name, prev, scheduler, cost_per_track=None):
    if cost_per_track==None:
        cost_per_track = lambda x : 10*x
    tracks = [99, 55, 43, 78, 4, 11, 89, 67, 1, 98, 45, 88, 60, 30, 77]
    totcost = 0
    ms_cost = 0
    print "\n*** Calculating %s:" % (name)

    for take in scheduler(prev, tracks):
        cost = math.fabs(take - prev)
        totcost +=cost
        print "From %2d to %2d --- %2d tracks" % (prev, take, cost)
        prev = take
        ms_cost += 1 # Time of reading one block (1ms)
        ms_cost += cost_per_track(cost) # Time used for track switching
        ms_cost += ((60*1000.0) / 7500.0) * 0.5 # 1/2 revolution of drive

    print "Total cost: %d track changes" % (totcost)
    print "Total cost %f ms or %f s" % (ms_cost, ms_cost/1000.0)

    return ms_cost


if __name__ == "__main__":
	s = measure("SCAN", 49, scan)
	f = measure("FCFS", 33, fcfs)

	print "fcfs is %f times slower than scan."  % (f/s)


