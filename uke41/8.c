#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>


int main(void)
{
	int fd = open("test.txt", O_CREAT | O_TRUNC | O_RDWR, 0777);
	char *msg = "Hello, World";
	
	if(fd == -1)
	{
		perror("open");
		return -1;
	}

	printf("FD=%d\n", fd);

	ssize_t retv = write(fd, msg, strlen(msg)); 
	if(retv != strlen(msg))
	{
		printf("Write only wrote %d bytes!\n", retv);
	}

	printf("Write wrote %d bytes!\n", retv);

	close(fd);

	return 0;
}
