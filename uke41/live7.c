#include <unistd.h>
#include <stdio.h>

int main(void)
{
	pid_t p = fork();

	if(p < 0)
	{
		perror("fork");
		return -1;
	}

	FILE* f = fopen("test.txt", "w");

	fprintf(f, "Hei, pid=%d\n", p);
	fclose(f);

	return 0;
}
