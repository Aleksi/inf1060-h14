# README #

Dette Git-repoet inneholder kode for plenumsdelen av kurset INF1060 høsten 2014.

Blant annet inkluderer dette kode som blir liveprogrammert under plenumsforelesningene og annen kode som gruppelærer lager til kurset.

### Hvordan laster jeg ned? ###

For å hente ned koden til egen maskin trenger du `git` installert, dette finnes allerede på Ifi-maskinene.

For å kopiere kildekoden bruker du kommandoen `git clone https://bitbucket.org/Aleksi/inf1060-h14.git`.
Det blir da laget en ny mappe `inf1060-h14` som inneholder koden som også finnes på nett.

Koden vil bli oppdatert hver uke, for å oppdatere til nyeste versjon må du være i mappen og utføre `git pull`:

    cd inf1060-h14
    git pull


### Hvem kan jeg spørre om hjelp? ###

Generelle C-spørsmål, eller spørsmål rundt obliger eller hjemmeeksamener kan sendes til orakel-epostlisten: inf1060-orakel (snabel a) ifi.uio.no.

Spørsmål rundt koden som ligger her, eller annet som blir gjennomgått i plenumstimene kan sendes til gruppelærer på aleksiml (snabel-a) ifi.uio.no