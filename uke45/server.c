#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>


int main(int argc, char* argv[])
{
	if(argc != 2)
	{
		printf("USAGE: %s [port-nr]\n", argv[0]);
		return 0;
	}

	int port = atoi(argv[1]);
	int fd = socket(AF_INET, SOCK_STREAM, 0);
	
	if(fd == -1)
	{
		perror("socket");
		return -1;
	}

	struct sockaddr_in binfo;
	memset(&binfo, 0, sizeof(binfo));

	binfo.sin_family = AF_INET;
	binfo.sin_port = htons(port); /* Finnes tilsvarende htonl (32-bits tall) */
	binfo.sin_addr.s_addr = INADDR_ANY;

	int retv = bind(fd, (struct sockaddr*)&binfo, sizeof(binfo));

	if(retv != 0)
	{
		perror("bind");
		return -2;
	}

	retv = listen(fd, 5);
	if(retv != 0)
	{
		perror("listen");
		return -3;
	}
	
	struct sockaddr_in client_info;
	memset(&client_info, 0, sizeof(client_info));
	socklen_t info_len = sizeof(client_info);

	int client_fd = accept(fd, (struct sockaddr*)&client_info, &info_len); 
	if(client_fd == -1)
	{
		perror("accept");
		return -4;
	}

	printf("Accepted client! Remote port=%d\n", ntohs(client_info.sin_port));

	while(1)
	{
		char buf[10];
		ssize_t rd = recv(client_fd, buf, sizeof(buf)-1, 0); 
		if(rd > 0)
		{
			buf[rd] = 0;
			printf("Recvd %zd bytes: %s\n", rd, buf);
		} else {
			break;
		}

		ssize_t sent = send(client_fd, "Hello, World\n", 13, 0);
		printf("Sent %zd bytes to client on fd %d\n", sent, client_fd);
	}
	close(client_fd);
	close(fd);

	return 0;
}
