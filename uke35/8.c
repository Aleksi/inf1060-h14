#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	int in;
	int fot,in2;

	while(1)
	{
		printf("Inches: ");
		if(scanf("%d", &in) != 1)
			return 0;
		/*
		 * scanf som brukt her  blir konseptuelt det samme som:
		char *a = malloc(1024);
		fgets(a, 1024, stdin);
		in = atoi(a);
		*/
		if(in == 0) return 0;

		fot = in/12;
		in2 = in%12;

		printf("%d inches is %d foot and %d inches.\n", in, fot, in2);
	}

}
