#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int is_number(const char* arg)
{
	int i;

	for(i = 0; i < strlen(arg); i++)
	{
		if(!(arg[i] >= '0' && arg[i] <= '9')) 
			return 0;
	}

	return 1;
}

int main(int argc, char* argv[])
{
	if(argc != 2)
	{
		fprintf(stderr, "USAGE: %s 'tekst'\n", argv[0]);
		return -1;
	}

	char *msg = argv[1];

	printf("Input: %s\n", msg);

	if(is_number(msg))
	{
		printf("Is number: %d\n", atoi(msg));
	}

	return 0;
}
