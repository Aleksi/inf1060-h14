#include <stdio.h>
#include <string.h>

/* 
 * Makroer er enkle 'søk-og-erstatt' snutter som kompilatoren gjør for deg.
 * De kan gjøre noe kode enklere, men skaper ofte problemer. Prøv å unngå makroer
 * til annet enn å definere konstanter (f.eks. #define TRUE 1).
 */

#define STREQ(s1, s2) (strcmp((s1),(s2)) == 0)

int main(void)
{
	if(STREQ("abcd", "abc"))
		printf("equal\n");
		
}
