#include <stdio.h>
#include <limits.h>


/*
 * Grunnen til at summen blir gal er at summen blir for stor
 * til å bli representert av typen vi bruker.
 *
 * Vanligvis vil en variabel av typen `short` bestå er 16 bit.
 * Ett av disse er fortegnsbittet. 20 000 + 20 000 blir mer enn
 * ett 15 bits tall han inneholde, dermed vil resultatet sette
 * fortegnsbittet.
 *
 * 20000 = 0b0100111000100000
 *           ^- Fortegnsbit, 0 positivt 1 negativt
 * 40000 = 0b1001110001000000
 * 
 * For å finne ut hvilket negativt tall dette er må man lese om 'twos complement'.
 * 
 * Koden under bruker 'int' da vi kan få GCC til å kræsje programmet om vi flyter over.
 * Kompiler med 'gcc -m32 -ftrapv 3.c -o 3' og kjør med './3', programmet vil gi output "Aborted".
 * Med en debugger kan man da finne ut hvor feilen har skjedd og rette opp i dette.
 *
 * En annen måte å sjekke dette på er å sjekke følgende to ting:
 * Hvis du legger sammen to positivt tall skal resultatet alltid være positivt.
 * Hvis du legger sammen to negative tall skal resultatet alltid være negativt.
 * Dersom dette ikke stemmer (f.eks. du plusser sammen to positive tall og får negativt resultat)
 * så har det skjedd 'overflyt'.
 */

int main(void)
{
	int a, b, sum;

	a = INT_MAX;  b = 1;  sum = a+b;
	printf("%d + %d = %d\n", a, b, sum);
}
