#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
 * Alternativ implementasjon av oppgave 1 som bruker `strtol`
 * istedenfor våre egne funksjoner + atoi
 */

int main(int argc, char* argv[])
{
	char *msg;
	char *end;
	int heltall;
	double flyttall;

	/*
	 * Se at vi har fått riktig antall argumenter
	 */
	if(argc != 2)
	{
		fprintf(stderr, "USAGE: %s 'tekst'\n", argv[0]);
		return -1;
	}

	msg = argv[1];
	printf("Input: %s\n", msg);

	/*
	 * Se om input er ett tall, hvis det ikke er ett tall, 
	 * sjekk om det er flyttall.
	 */

	heltall = strtol(msg, &end, 10);
	
	if(end[0] == '\0')
	{
		printf("Is number! It is: %d\n", heltall);
	} else {
		flyttall = strtod(msg, &end);
		if(end[0] == '\0')
			printf("Is float! It is: %f\n", flyttall);
	}

	printf("First invalid char is '%c'\n", end[0]);

	return 0;
}
