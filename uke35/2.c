#include <stdio.h>
#include <string.h>

char strgetc(const char s[], int pos)
{
	if(pos < 0 || pos >= strlen(s))
		return -1;

	return s[pos];
}


int main(void)
{
	const char *str = "abcd";
	printf("String %s: \n", str);

	printf("Pos -1: %d\n", strgetc(str, -1));
	printf("Pos 1: %d %c\n", strgetc(str, 1), strgetc(str, 1));
	printf("Pos 100: %d\n", strgetc(str, 100));

	return 0;
}
