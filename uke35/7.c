#include <stdio.h>
#include <string.h>

int strcmpx(const char s1[], const char s2[]){
	int s1len = strlen(s1);
	int s2len = strlen(s2);
	int i;

	if(s1len < s2len)
		return -1;

	if(s1len > s2len)
		return 1;

	for(i = 0; i < s1len; ++i)
	{
		int sum = s1[i] - s2[i];
		if(sum != 0)
			return sum;
	}

	return 0;
}

void test(const char s1[], const char s2[]){
	printf("strcmpx(\"%s\", \"%s\") gir %d, strcmp gir %d\n",
			s1, s2, strcmpx(s1,s2), strcmp(s1,s2));
}

int main(){
	test("Abc", "Ab");
	test("Abc", "Abc");
	test("Abc", "Abcd");
	return 0;
}
