#include <stdio.h>
#include <stdlib.h>


struct liste
{
	int a;
	struct liste* n;
};

void add(struct liste** first, int elem)
{
	//del(first, 1); <- ville fungert
	struct liste* nn = malloc(sizeof(struct liste));
	nn->a = elem;
	nn->n = NULL;
	
	while(*first != NULL)
		first = &((*first)->n);

	*first = nn;
}

void del(struct liste** first, int elem)
{
	while(*first != NULL && (*first)->a != elem)
		first = &((*first)->n);

	if(*first == NULL)
		return;

	*first = (*first)->n;
}

void prlist(struct liste *l)
{
	if(l == NULL)
		return;

	printf("%d\n", l->a);
	prlist(l->n);

	//del(&l, 1); <- ville ikke fungert
	//del(&l, 2); <- fungerer
}

int main(void)
{
	struct liste* f = NULL;

	add(&f, 1);
	add(&f, 2);
	add(&f, 3);

	prlist(f);

	printf("DELETE->\n");
	del(&f, 2);
	prlist(f);

	printf("DELETE2->\n");
	del(&f, 1);
	prlist(f);

	printf("DELETE3->\n");
	del(&f, 3);
	prlist(f);


	return 0;
}
