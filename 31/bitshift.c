#include <stdio.h>
#include <stdint.h>

void print_bin(uint8_t a)
{
	int i;
	for(i = 7; i >= 0; --i)
	{
		printf("%d", (a>>i)&1);
	}
	printf("\n");
}

void print_shift(uint8_t a)
{
	int i;
	for(i = 0; i < 8; ++i)
	{
		printf("I=%d: ", i);
		print_bin(a<<i);
	}
}

int main(void)
{
	//uint8_t a = 0;

	/*for(a = 0; a < 16; ++a)
	{
		printf("%x\n", a);
		printf("Binary: ");
		print_bin(a);
	}*/

	//print_shift(0xFF);

	uint8_t a,b,c;

	a = 0xF8;
	b = 0x0F;

	/*print_bin(a);
	print_bin(b);

	c = a | b;
	print_bin(c);*/

	/*
	a = a | b;
	a |= b;
	*/

	//Xor-swap
	printf("a: "); print_bin(a);
	printf("b: "); print_bin(b);
	a = a^b;
	printf("a: "); print_bin(a);
	b = b^a; // b = b^a^b => b = a
	printf("b: "); print_bin(b);
	a = a^b; // a = a^b^b^a^b => a^b^a => b
	printf("a: "); print_bin(a);

	//a = 0;
	//movl $0, %eax => 0xzz 0x00 0x00 0x00 0x00
	//xorl %eax, %eax

	return 0;
}
