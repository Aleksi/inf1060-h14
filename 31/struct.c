#include <stdio.h>
#include <stddef.h>

struct test2
{
	int c;
	int d;
};

struct test
{
	char a;
	int b;
	//struct test2 *t2;
};

void zero(struct test2 *t)
{
	t->c = 0;
}

void add_2(struct test2 *t)
{
	t->c += 2;
	t->d += 2;

	zero(t);
	//t->c == 0
}

int main(void)
{
	struct test t;
	struct test *tp;

	//tp->a
	//(*tp).a

	//tp->t2->d
	//t.t2->d

	t.b = 0;
	
	printf("t=%p\n", &t);
	printf("t.a=%p\n", &t.a);
	printf("t.b=%p\n", &t.b);

	printf("offsetof(t,a)=%d\n", offsetof(struct test, a));
	printf("offsetof(t,b)=%d\n", offsetof(struct test, b));

	FILE* f = fopen("test.dat", "w");
	fwrite(&t, sizeof(t), 1, f);
	fclose(f);

	//fread(&t, sizeof(t), 1, f);
	
	return 0;
}
