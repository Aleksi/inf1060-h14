#include <stdio.h>


int main(void)
{
	char abc_bitmap = 0;
	char abc[8];
	
	// Legge inn element
	abc[0] = 'a';
	abc_bitmap |= 1<<7;


	//Fjerne element
	abc[0] = 0;
	abc_bitmap &= ~(1<<7);


	//Hente ut bit
	char a = abc_bitmap & (1<<7);
	(void)a;

#ifdef DEBUG
	fprintf(stderr, "Hello, World\n");
#endif

	return 0;
}
