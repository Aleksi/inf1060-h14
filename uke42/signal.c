#include <signal.h>
#include <stdio.h>


void handler(int sig)
{
	signal(2, handler);
	printf("Got signal: %d\n", sig);
}

int main(void)
{
	signal(2, handler);
	while(1);
	return 0;
}
