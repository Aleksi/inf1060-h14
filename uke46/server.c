#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <netdb.h>

typedef struct client_conn
{
	int cfd;
} client_t;

client_t clients[100];

void accept_client(int listen_fd)
{
	int cfd = accept(listen_fd, NULL, NULL);
	printf("New client: fd=%d\n", cfd);

	int i;
	for(i = 0; i < 100; ++i)
	{
		if(clients[i].cfd == -1)
		{
			clients[i].cfd = cfd;
			printf("Stored as client %d\n", i);
			return;
		}
	}

	printf("Could not find space for new client!\n");
	close(cfd);

	return;
}

void init_clients(void)
{
	int i;
	for(i = 0; i < 100; ++i)
		clients[i].cfd = -1;
}

int main(int argc, char* argv[])
{
	if(argc != 3)
	{
		printf("USAGE: %s [ip-addr] [port]\n", argv[0]);
		return 0;
	}
	
	init_clients();

	const char* ipaddr = argv[1];
	const char* port = argv[2];

	struct addrinfo hints,*result;
	memset(&hints, 0, sizeof(hints));

	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	int retv = getaddrinfo(ipaddr, port, &hints, &result); 

	if(retv != 0)
	{
		printf("getaddrinfo failed: %s\n", gai_strerror(retv));
		return -1;
	}

	assert(result);

	int listen_sock = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

	if(listen_sock == -1)
	{
		perror("socket");
		freeaddrinfo(result);
		return -2;
	}

	retv = bind(listen_sock, result->ai_addr, result->ai_addrlen);
	freeaddrinfo(result);
	if(retv != 0)
	{
		perror("bind");
		return -3;
	}

	retv = listen(listen_sock, 5);
	if(retv != 0)
	{
		perror("listen");
		return -4;
	}

	while(1)
	{
		fd_set readfds;	
		int highest,i;

		FD_ZERO(&readfds);
		FD_SET(listen_sock, &readfds);
		highest = listen_sock;

		for(i = 0; i < 100; ++i)
		{
			if(clients[i].cfd == -1)
				continue;

			if(clients[i].cfd > highest)
				highest = clients[i].cfd;

			FD_SET(clients[i].cfd, &readfds);
		}

		int numevents = select(highest+1, &readfds, NULL, NULL, NULL);

		printf("select returned, numevents=%d\n", numevents);
		
		if(numevents == -1)
		{
			perror("select");
			exit(-1);
		}
		else if(numevents == 0)
		{
			perror("select");
			continue;
		} 
		else
		{
			if(FD_ISSET(listen_sock, &readfds))
			{
				accept_client(listen_sock);
				numevents--;
			}

			for(i = 0; i < 100 && numevents > 0; ++i)
			{
				if(clients[i].cfd == -1)
					continue;

				if(FD_ISSET(clients[i].cfd, &readfds))
				{
					char buf[100];
					ssize_t recv_len = recv(clients[i].cfd, buf, 100, 0);	
					
					if(recv_len < 0)
					{
						perror("recv");
						exit(-1);
					}
					else if(recv_len == 0)
					{
						perror("recv");
						printf("Client number %d closed\n", i);
						close(clients[i].cfd);
						clients[i].cfd = -1;
					}
					else
					{
						printf("Received %zd bytes from client %d\n", recv_len, i);
					}
					
					numevents--;
				}
			}
		}
	}

	return 0;
}
