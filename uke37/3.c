#include <stdio.h>
#include <string.h>

//FILE* stdin = ... allerede deklarert i stdio.h, leser fra terminalen.

int main(void)
{
	char filnavn[100];
	char buf[1024];
	FILE* fil;
	int i;

	while(fgets(filnavn, 100, stdin) != NULL)
	{
		filnavn[strlen(filnavn)-1] = 0;

		if(strcmp(filnavn, "QUIT") == 0)
			return 0;
		
		fil = fopen(filnavn, "r");
		if(!fil)
		{
			perror("Kunne ikke åpne fil");
			continue;
		}
		
		i = 1;
		while(fgets(buf, 1024, fil) != NULL)
		{
			if(i%2 == 0)
				printf("%s", buf);

			i++;
		}

		fclose(fil);
	}

	return 0;
}
