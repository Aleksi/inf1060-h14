#define INFO 0
#define ERROR 1
#define DEBUG 2

const int is_debug = 0;

void log_msg(int severity, const char *msg)
{
	if(severity == ERROR)
		fprintf(stderr, ...);
	if(severity == DEBUG)
		if(is_debug)
			fprintf(stderr, ...);

	if(severity == INFO)
		printf(...);
}
