#ifndef MYMATH_H
#define MYMATH_H

int mymath_sum(int a, int b);
int mymath_mul(int a, int b);
int mymath_div(int a, int b);

#endif
