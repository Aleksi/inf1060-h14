#include <stdio.h>


int main(int argc, char* argv[])
{
	char buf[100];

	if(argc != 4)
	{
		fprintf(stderr, "USAGE: %s [navn] [alder] [filnavn]\n", argv[0]);
		return -1;
	}

	sprintf(buf, "Hello %s, you are %s years old.", argv[1], argv[2]);	
	printf("%s\n", buf);

	FILE* f = fopen(argv[3], "w");

	if(!f)
	{
		perror("fopen");
		return -1;
	}

	fputs(buf, f);
	fclose(f);
	
	return 0;
}
